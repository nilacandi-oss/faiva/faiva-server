/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi All rights reserved.
 *
 * This source code is licensed under the license found in the LICENSE.txt file in the root
 * directory of this source tree.
 *******************************************************************************/
package aero.facilis.xmlvalidator.api;

import static org.assertj.core.api.Assertions.assertThat;
import java.io.File;
import java.net.URL;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import aero.facilis.xmlvalidator.model.Report;
import lombok.extern.log4j.Log4j2;

/**
 * @author khainglay
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {XMLValidator.class})
@Log4j2
public class XMLValidatorTest {

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {}

  @Test
  public void testValidateXMLWithSAX_Valid() throws Exception {
    // prepare data
    final URL featureDocURL = this.getClass().getResource("/xml/MMR-RunwayDirection.xml");
    final File featureDoc = new File(featureDocURL.getFile());

    // Provide our own catalog with the standard AIXM schema
    final URL catalogUrl = this.getClass().getResource("/schemas/catalog.xml");
    final File catalog = new File(catalogUrl.getFile());

    final List<Report> result = XMLValidator.validateXMLWithSAX(featureDoc, catalog, "Test Schema");

    // If validation succeeds, no results will come out
    assertThat(result.isEmpty()).isTrue();
  }

  @Test
  public void testValidateXMLWithSAX_Invalid() throws Exception {
    // prepare data, there are duplicate GML IDs
    final URL featureDocURL =
        this.getClass().getResource("/xml/MMR-RunwayDirection_Invalid_Version.xml");
    final File featureDoc = new File(featureDocURL.getFile());

    // Provide our own catalog with the standard AIXM schema
    final URL catalogUrl = this.getClass().getResource("/schemas/catalog.xml");
    final File catalog = new File(catalogUrl.getFile());

    final List<Report> result = XMLValidator.validateXMLWithSAX(featureDoc, catalog, "Test Schema");
    log.info("From testValidateXMLWithSAX_Invalid(): " + result.get(0).getErrorMessage());

    assertThat(result.get(0).isValid()).isFalse();
  }

}
