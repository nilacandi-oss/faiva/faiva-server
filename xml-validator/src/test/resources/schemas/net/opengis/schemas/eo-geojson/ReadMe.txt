OGC(r) EO GeoJSON Schema ReadMe.txt
===================================

OGC(r) EO Dataset Metadata GeoJSON(-LD) Encoding Standard (eo-geojson)
-----------------------------------------------------------------------

More information may be found at
 http://www.opengeospatial.org/standards/

The most current schema are available at http://schemas.opengis.net/
and all OGC schemas may be downloaded in a complete bundle from
http://schemas.opengis.net/SCHEMAS_OPENGIS_NET.zip

* Latest version is: http://schemas.opengis.net/eo-geojson/ *

-----------------------------------------------------------------------

2019-08-31  Yves Coene
  + v1.0: Added EO GeoJSON 1.0.0 as eo-geojson/1.0 from OGC 17-003r1

 Note: Check each OGC numbered document for detailed changes.

-----------------------------------------------------------------------

Policies, Procedures, Terms, and Conditions of OGC(r) are available
  http://www.opengeospatial.org/ogc/legal/ .

OGC and OpenGIS are registered trademarks of Open Geospatial Consortium.

Copyright (c) 2019 Open Geospatial Consortium.

-----------------------------------------------------------------------

