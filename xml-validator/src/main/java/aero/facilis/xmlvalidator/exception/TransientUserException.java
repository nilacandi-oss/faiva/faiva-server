/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.xmlvalidator.exception;

/**
 * A type of UserException for a problem that might not last. The user may want to try again after a
 * short while.
 * 
 * @author Benoit Maisonny
 *
 */
public class TransientUserException extends UserException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;


  public TransientUserException(Key key) {
    super(key);
  }

  public TransientUserException(Key key, Throwable t) {
    super(key, t);
  }

  public TransientUserException(Key key, Object[] args) {
    super(key, args);
  }

  public TransientUserException(Key key, Throwable t, Object[] args) {
    super(key, t, args);
  }
}
