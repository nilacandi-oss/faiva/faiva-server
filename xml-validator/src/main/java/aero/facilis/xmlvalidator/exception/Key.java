/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.xmlvalidator.exception;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * A message key used for translation of an exception message.
 * 
 * @author Benoit Maisonny
 *
 */
public interface Key {
  /**
   * @return The message key, which is used to obtain a localised message.
   */
  @Override
  String toString();


  /**
   * @return The message key name. For a properties resource bundle, this is the property name, not
   *         including the bundle name. Note: implementations of this interface are normally made
   *         using enums, and the name() method is pre-defined by the Java language for enums.
   */
  String name();

  default String getBundleName() {
    return this.getClass().getName();
  }


  /**
   * Translate this message key using the system default locale.
   * 
   * @return Translated message.
   */
  default String translate() {
    return translate(null, null, null);
  }


  /**
   * Translate this message key using the system default locale.
   * 
   * @param args Message arguments. May be null.
   * @return Formatted and translated message.
   */
  default String translate(final Object[] args) {
    return translate(args, null, null);
  }

  /**
   * Translate this message key.
   * 
   * @param locale The locale to use for translation. If null, the system default locale is used.
   * @return Translated message.
   */
  default String translate(final Locale locale) {
    return translate(null, locale, null);
  }

  /**
   * Translate this message key.
   * 
   * @param args Message arguments.
   * @param placeHolder
   * @return Translated message.
   */
  default String translate(final Object[] args, String placeHolder) {
    return translate(args, null, placeHolder);
  }

  /**
   * Translate this message key.
   * 
   * @param args Message arguments. May be null.
   * @param locale The locale to use for translation. If null, the system default locale is used.
   * @return Formatted and translated message.
   */
  default String translate(final Object[] args, final Locale locale, final String placeHolder) {
    final Locale myLocale = locale == null ? Locale.getDefault() : locale;
    // Get the resource bundle which stores translated messages and patterns
    final ResourceBundle messages = ResourceBundle.getBundle(getBundleName(), myLocale);

    String localisedMessage = messages.getString(name());
    // Combine placeHolder into message
    if (placeHolder != null)
      localisedMessage = MessageFormat.format(localisedMessage, placeHolder);

    if (args == null || args.length == 0) {
      // No arguments: translate directly
      return localisedMessage;
    }

    // With arguments: format the message
    MessageFormat formatter = new MessageFormat(localisedMessage, myLocale);
    return formatter.format(args);
  }
}
