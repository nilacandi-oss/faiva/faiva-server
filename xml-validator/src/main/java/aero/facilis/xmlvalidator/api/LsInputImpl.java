/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.xmlvalidator.api;

import java.io.InputStream;
import java.io.Reader;
import org.w3c.dom.ls.LSInput;

/**
 * Custom class that is for CrLsResourceResolver
 * 
 * @author Thiha Maung
 *
 */
public class LsInputImpl implements LSInput {

  private String publicId;
  private String systemId;
  private String baseURI;
  private InputStream byteStream;
  private Reader characterStream;
  private String stringData;
  private boolean certifiedText;
  private String encoding;

  public String getPublicId() {
    return publicId;
  }

  public void setPublicId(String publicId) {
    this.publicId = publicId;
  }

  public String getSystemId() {
    return systemId;
  }

  public void setSystemId(String systemId) {
    this.systemId = systemId;
  }

  public String getBaseURI() {
    return baseURI;
  }

  public void setBaseURI(String baseURI) {
    this.baseURI = baseURI;
  }

  public InputStream getByteStream() {
    return byteStream;
  }

  public void setByteStream(InputStream byteStream) {
    this.byteStream = byteStream;
  }

  public Reader getCharacterStream() {
    return characterStream;
  }

  public void setCharacterStream(Reader characterStream) {
    this.characterStream = characterStream;
  }

  public String getStringData() {
    return stringData;
  }

  public void setStringData(String stringData) {
    this.stringData = stringData;
  }

  public boolean getCertifiedText() {
    return certifiedText;
  }

  public void setCertifiedText(boolean certifiedText) {
    this.certifiedText = certifiedText;
  }

  public String getEncoding() {
    return encoding;
  }

  public void setEncoding(String encoding) {
    this.encoding = encoding;
  }

}
