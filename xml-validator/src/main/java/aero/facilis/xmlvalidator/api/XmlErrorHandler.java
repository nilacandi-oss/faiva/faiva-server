/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi All rights reserved.
 *
 * This source code is licensed under the license found in the LICENSE.txt file in the root
 * directory of this source tree.
 *******************************************************************************/
package aero.facilis.xmlvalidator.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import aero.facilis.xmlvalidator.model.Report;
import aero.facilis.xmlvalidator.model.ReportFlag;
import lombok.extern.log4j.Log4j2;

/**
 * @author khainglay
 * @author Thiha Maung
 *
 */
@Log4j2
public class XmlErrorHandler implements ErrorHandler {

  private final List<Report> reports;
  private final String schemaName;
  private Map<Object, Object> validationRuleMsgMap;

  public XmlErrorHandler(final String schemaName) {
    this.reports = new ArrayList<>();
    this.validationRuleMsgMap = new HashMap<>();
    this.schemaName = schemaName;
  }

  public List<Report> getReports() {
    return this.reports;
  }

  public String getSchemaName() {
    return this.schemaName;
  }

  public Map<Object, Object> getValidationRuleMsgMap() {
    return this.validationRuleMsgMap;
  }

  public void setValidationRuleMsgMap(final Map<Object, Object> validationRuleMsgMap) {
    this.validationRuleMsgMap = validationRuleMsgMap;
  }

  private String getLocation(final int lineNo, final int columnNo) {
    return "Line: " + lineNo + ", Column: " + columnNo;
  }

  @Override
  public void warning(final SAXParseException exception) throws SAXException {
    log.trace("WARNING: " + exception.getMessage());
    final Report report =
        new Report(this.schemaName, this.processValidationRuleMsg(exception.getMessage()),
            this.getLocation(exception.getLineNumber(), exception.getColumnNumber()),
            ReportFlag.WARNING.toString(), false);
    this.reports.add(report);
  }

  @Override
  public void error(final SAXParseException exception) throws SAXException {
    log.trace("ERROR: " + exception.getMessage());
    final Report report =
        new Report(this.schemaName, this.processValidationRuleMsg(exception.getMessage()),
            this.getLocation(exception.getLineNumber(), exception.getColumnNumber()),
            ReportFlag.ERROR.toString(), false);
    this.reports.add(report);
  }

  @Override
  public void fatalError(final SAXParseException exception) throws SAXException {
    log.trace("FATAL: " + exception.getMessage());
    final Report report =
        new Report(this.schemaName, this.processValidationRuleMsg(exception.getMessage()),
            this.getLocation(exception.getLineNumber(), exception.getColumnNumber()),
            ReportFlag.FATAL.toString(), false);
    this.reports.add(report);
  }

  private String processValidationRuleMsg(String originalMsg) {
    final String rule = originalMsg.substring(0, originalMsg.indexOf(':'));
    if (!rule.isEmpty()) {
      final String msg = (String) this.validationRuleMsgMap.get(rule);
      originalMsg = !StringUtils.isEmpty(msg) ? originalMsg + " Suggestion: " + msg : originalMsg;
    }
    return originalMsg;
  }

}
