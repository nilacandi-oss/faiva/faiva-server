/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi All rights reserved.
 *
 * This source code is licensed under the license found in the LICENSE.txt file in the root
 * directory of this source tree.
 *******************************************************************************/
package aero.facilis.xmlvalidator.api;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import org.apache.xml.resolver.CatalogManager;
import org.apache.xml.resolver.tools.CatalogResolver;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import aero.facilis.xmlvalidator.exception.UserException;
import aero.facilis.xmlvalidator.model.ErrorMessages;
import aero.facilis.xmlvalidator.model.Report;
import aero.facilis.xmlvalidator.model.ReportFlag;
import lombok.extern.log4j.Log4j2;

/**
 * Implementation to validate XML against schema.
 *
 * @author khainglay
 * @author Thiha Maung
 *
 */
@Service
@Log4j2
public class XMLValidator {

  static final String JAXP_SCHEMA_LANGUAGE =
      "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
  static final String JAXP_SCHEMA_SOURCE = "http://java.sun.com/xml/jaxp/properties/schemaSource";
  static final String AIXM_BASIC_MESSAGE_XSD_URL =
      "http://www.aixm.aero/schema/5.1.1/message/AIXM_BasicMessage.xsd";

  private XMLValidator() {
    // throw new IllegalStateException("XMLValidator")
  }

  public static List<Report> validateXMLWithSAX(final File xml, final File catalogUri,
      final String schemaName) throws UserException {

    final List<Report> reports = new ArrayList<>();

    // 1. Create catalog resolver
    final CatalogManager cm = new CatalogManager();
    cm.setUseStaticCatalog(false);
    cm.setCatalogFiles(catalogUri.toString());
    log.debug("catalogUri: " + catalogUri.toString());
    final CatalogResolver cr = new CatalogResolver(cm);

    // 2. Create SAX parser
    final XMLReader reader;
    try {
      // Build SAX parser factory
      final SAXParserFactory factory = SAXParserFactory.newInstance();
      factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
      factory.setValidating(true);
      factory.setNamespaceAware(true);

      // Build XMLReader
      reader = factory.newSAXParser().getXMLReader();
      reader.setProperty(JAXP_SCHEMA_LANGUAGE, XMLConstants.W3C_XML_SCHEMA_NS_URI);

      // set schema
      final InputSource schemaIS = cr.resolveEntity(null, AIXM_BASIC_MESSAGE_XSD_URL);
      reader.setProperty(JAXP_SCHEMA_SOURCE, schemaIS);

      // set catalog resolver
      reader.setEntityResolver(cr);

    } catch (final SAXException | ParserConfigurationException e) {
      log.error("SCHEMA : " + e.getMessage()); // problem in the XSD itself
      throw new UserException(ErrorMessages.INVALID_SCHEMA, e);
    }
    // 3. Set custom error handler so that we can collect warnings and errors
    final XmlErrorHandler xmlErrorHandler = new XmlErrorHandler(schemaName);
    xmlErrorHandler.setValidationRuleMsgMap(createValidationRuleMsgMap());
    reader.setErrorHandler(xmlErrorHandler);

    try {
      final BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(xml));
      reader.parse(new InputSource(inputStream));
      final List<Report> reportResult = xmlErrorHandler.getReports();
      if (!reportResult.isEmpty()) {
        reports.addAll(reportResult);
      }
    } catch (final IOException e) {
      log.error("IOException occurs. ", e);
      reports.add(new Report(schemaName, "IOException: " + e.getLocalizedMessage(), "",
          ReportFlag.ERROR.toString(), false));

    } catch (final SAXException e) {
      log.error("Validation fail!");
      reports.add(new Report(schemaName, "SAXException: " + e.getLocalizedMessage(), "",
          ReportFlag.ERROR.toString(), false));
    }
    return reports;
  }

  /**
   * Create ValidationRuleMsgMap from Properties Files
   * 
   * @return Map
   */
  private static Map<Object, Object> createValidationRuleMsgMap() {

    Map<Object, Object> validationRuleMsgMap = new LinkedHashMap<>();
    try {
      URL url = XMLValidator.class.getResource("/application.properties");
      Properties properties = new Properties();
      properties.load(new FileInputStream(url.getPath()));

      InputStream ioRule =
          XMLValidator.class.getResourceAsStream(properties.getProperty("validation.rulePath"));
      InputStream ioMsg =
          XMLValidator.class.getResourceAsStream(properties.getProperty("validation.msgPath"));

      final Properties propRule = new Properties();
      propRule.load(ioRule);
      final Set<Object> keys = propRule.keySet();

      final Properties propMsg = new Properties();
      propMsg.load(ioMsg);

      for (final Object obj : keys) {
        validationRuleMsgMap.put(propRule.get(obj), propMsg.get(obj));
      }
    } catch (final IOException e) {
      log.error(e);
    }
    return validationRuleMsgMap;
  }

  // SchemaFactory implementation. It is for ref later.
  //
  // public static Report validateXMLWithSAX(final File xml, final File catalogUri)
  // throws UserException {
  // // 1. Build CatalogResolver
  // final CatalogManager cm = new CatalogManager();
  // cm.setUseStaticCatalog(false);
  // cm.setCatalogFiles(catalogUri.toString());
  // log.debug("catalogUri: " + catalogUri.toString());
  // final CatalogResolver cr = new CatalogResolver(cm);
  //
  // // 2. Build SchemaFactory
  // final Source xmlFile;
  // final Validator validator;
  // try {
  // final SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
  // // factory.setResourceResolver(new CrLsResourceResolver(cr));
  // factory.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
  //
  // // final InputSource schemaIS = cr.resolveEntity(null, AIXM_BASIC_MESSAGE_XSD_URL)
  // // final Source schemaFile = new StreamSource(schemaIS.getByteStream())
  // xmlFile = new StreamSource(new FileInputStream(xml));
  //
  // final Schema schema = factory.newSchema();
  // validator = schema.newValidator();
  //
  // } catch (final SAXException | FileNotFoundException e) {
  // log.error("SCHEMA : " + e.getMessage()); // problem in the XSD itself
  // throw new UserException(ErrorMessages.INVALID_SCHEMA, e);
  // }
  //
  // // 3. Set custom error handler so that we can collect warnings and errors
  // final XmlErrorHandler xmlErrorHandler = new XmlErrorHandler();
  // validator.setErrorHandler(xmlErrorHandler);
  // validator.setResourceResolver(new CrLsResourceResolver(cr));
  //
  // try {
  // validator.validate(xmlFile);
  // return new Report(true);
  //
  // } catch (final IOException e) {
  // throw new InternalException(ErrorMessages.INTERNAL_ERROR, e);
  //
  // } catch (final SAXException e) {
  // return new Report(xmlErrorHandler.getErrors(), false);
  // }
  // }

}
