/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.xmlvalidator.api;

import org.apache.xml.resolver.tools.CatalogResolver;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.InputSource;

/**
 * Custom resolver that resolves CatalogResolver as LSResourceResolver
 * 
 * @author Thiha Maung
 *
 */
public class CrLsResourceResolver implements LSResourceResolver {

  private CatalogResolver catalogResolver;

  public CrLsResourceResolver(final CatalogResolver catalogResolver) {
    this.catalogResolver = catalogResolver;
  }

  @Override
  public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId,
      String baseURI) {
    InputSource is = catalogResolver.resolveEntity(namespaceURI, systemId);

    LsInputImpl lsInput = new LsInputImpl();
    if (is != null) {
      lsInput.setPublicId(is.getPublicId());
      lsInput.setSystemId(is.getSystemId());
      lsInput.setEncoding(is.getEncoding());
      lsInput.setByteStream(is.getByteStream());
    }
    return lsInput;
  }

}
