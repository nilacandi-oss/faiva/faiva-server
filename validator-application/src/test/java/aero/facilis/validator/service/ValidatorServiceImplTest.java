/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi All rights reserved.
 *
 * This source code is licensed under the license found in the LICENSE.txt file in the root
 * directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.io.File;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import com.amazonaws.services.s3.AmazonS3;
import aero.facilis.schematronvalidator.model.Report;
import aero.facilis.validator.model.AixmDoc;
import aero.facilis.validator.model.AixmSchema;
import aero.facilis.validator.model.AixmValidateBody;
import aero.facilis.validator.model.BizRule;
import aero.facilis.validator.model.ValidationOption;
import aero.facilis.validator.properties.DOMetadataProperties;
import aero.facilis.validator.properties.DOProperties;
import aero.facilis.validator.properties.KeycloakProperties;
import aero.facilis.validator.properties.SchemaVersionProperties;
import aero.facilis.validator.properties.ValidatorProperties;

/**
 * @author khainglay
 *
 */
@RunWith(SpringRunner.class)
@EnableConfigurationProperties
@SpringBootTest(classes = {ValidatorServiceImpl.class, ValidatorService.class, DOProperties.class,
    DOMetadataProperties.class, SchemaVersionProperties.class, KeycloakProperties.class,
    ValidatorProperties.class})
public class ValidatorServiceImplTest {

  @Autowired
  private ValidatorService validatorService;
  @Autowired
  private ValidatorProperties validatorProp;
  @Autowired
  private DOProperties doProperties;
  @Autowired
  private SchemaVersionProperties schemaVarProp;
  @Autowired
  private AmazonS3 s3Client;

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {}

  /**
   * Test method for
   * {@link aero.facilis.validator.service.ValidatorServiceImpl#uploadDocument(org.springframework.web.multipart.MultipartFile)}.
   *
   * @throws Exception
   */
  // @Test
  // Test will fail because of empty DOSpaces credentials. So test is skipped intentionally.
  public final void testUploadDocument() throws Exception {
    // prepare data
    final MockMultipartFile file = new MockMultipartFile("uploadFile", "MMR-RunwayDirection.xml",
        "application/xml", this.getClass().getResourceAsStream("/xml/MMR-RunwayDirection.xml"));

    final AixmDoc doc = this.validatorService.uploadDocument(file);

    assertNotNull(doc.getIdentifier());
    assertEquals("MMR-RunwayDirection.xml", doc.getName());
  }

  /**
   * Test method for
   * {@link aero.facilis.validator.service.ValidatorServiceImpl#validateXMLWithPureSchematron(aero.facilis.validator.model.AixmValidateBody)}.
   *
   * @throws Exception
   */
  @Test
  public final void testValidateXMLWithPureSchematron() throws Exception {
    // Prepare data
    final MockMultipartFile file = new MockMultipartFile("uploadFile", "AIP-DS-invalid2.xml",
        "application/xml", this.getClass().getResourceAsStream("/xml/AIP-DS-invalid2.xml"));
    final AixmDoc doc = this.validatorService.uploadDocument(file);
    final UUID docId = UUID.fromString("7b4adf98-77cd-411f-817d-38fa537065e8");

    final AixmValidateBody body = new AixmValidateBody();
    body.doc(doc);
    body.addBizRulesItem(
        new BizRule(docId, "AIXM-5.1-AIP-tables-BR.sch", "AIXM-5.1-AIP-tables-BR.sch"));

    final List<Report> result = this.validatorService.validateXMLWithPureSchematron(body,
        new File(this.getClass().getResource("/xml/AIP-DS-invalid2.xml").toURI()));

    boolean isValid = true;
    for (Report report : result) {
      if (!report.isValid()) {
        isValid = false;
        break;
      }
    }
    assertThat(isValid).isTrue();
  }

  @Test
  public final void testValidateXMLWithSchema() throws Exception {
    // prepare data
    final MockMultipartFile file = new MockMultipartFile("uploadFile", "AIP-DS-invalid2.xml",
        "application/xml", this.getClass().getResourceAsStream("/xml/AIP-DS-invalid2.xml"));
    final AixmDoc doc = this.validatorService.uploadDocument(file);
    final UUID docId = UUID.fromString("4c42bb0d-578f-4bdf-abaf-b70d8dc6a441");

    final AixmValidateBody body = new AixmValidateBody();
    body.doc(doc);
    body.addAixmSchemaItem(
        new AixmSchema(docId, "AIXM-5.1 original schema", "AIXM-5.1 original schema"));

    // call service
    final List<aero.facilis.xmlvalidator.model.Report> reports =
        this.validatorService.validateXMLWithSchema(body,
            new File(this.getClass().getResource("/xml/AIP-DS-invalid2.xml").toURI()));

    assertThat(reports.isEmpty()).isTrue();
  }

  @Test
  public final void testGetValidationOptions() throws Exception {

    final Set<ValidationOption> options = this.validatorService.getValidationOptions(true,
        schemaVarProp.getAixm511BasicMsgNsUrlCode());

    assertNotNull(options);
    assertThat(options.isEmpty()).isFalse();
  }
}
