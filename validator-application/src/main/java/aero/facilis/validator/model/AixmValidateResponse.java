/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.model;

import java.util.Objects;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * AixmValidationResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen",
    date = "2022-12-16T07:05:00.434Z[GMT]")
public class AixmValidateResponse {
  @JsonProperty("taskId")
  private UUID taskId = null;

  @JsonProperty("message")
  private String message = null;

  public AixmValidateResponse(@JsonProperty("taskId") final UUID taskId,
      @JsonProperty("message") final String message) {
    this.taskId = taskId;
    this.message = message;
  }

  /**
   * Get taskId
   *
   * @return taskId
   **/
  @Schema(description = "")
  public UUID getTaskId() {
    return this.taskId;
  }

  public void setTaskId(final UUID taskId) {
    this.taskId = taskId;
  }

  /**
   * Get message
   *
   * @return message
   **/
  @Schema(description = "")
  public String getMessage() {
    return this.message;
  }

  public void setMessage(final String name) {
    this.message = name;
  }


  @Override
  public boolean equals(final java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final AixmValidateResponse aixmValidateResponse = (AixmValidateResponse) o;
    return Objects.equals(this.taskId, aixmValidateResponse.taskId)
        && Objects.equals(this.message, aixmValidateResponse.message);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.taskId, this.message);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("class AixmValidateResponse {\n");

    sb.append("    taskId: ").append(toIndentedString(this.taskId)).append("\n");
    sb.append("    message: ").append(toIndentedString(this.message)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(final java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
