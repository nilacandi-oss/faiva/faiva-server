/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.model;

import java.util.Objects;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * AixmDoc
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen",
    date = "2022-12-16T07:05:00.434Z[GMT]")
public class AixmDoc {
  @JsonProperty("identifier")
  private UUID identifier = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("version")
  private String version = null;

  public AixmDoc(@JsonProperty("identifier") final UUID identifier,
      @JsonProperty("name") final String name, @JsonProperty("version") final String version) {
    this.identifier = identifier;
    this.name = name;
    this.version = version;
  }

  /**
   * Get identifier
   *
   * @return identifier
   **/
  @Schema(description = "")
  @Valid
  public UUID getIdentifier() {
    return this.identifier;
  }

  public void setIdentifier(final UUID identifier) {
    this.identifier = identifier;
  }

  /**
   * Get name
   *
   * @return name
   **/
  @Schema(description = "")
  public String getName() {
    return this.name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  /**
   * Get version
   *
   * @return version
   **/
  @Schema(description = "")
  public String getVersion() {
    return this.version;
  }

  public void setVersion(final String version) {
    this.version = version;
  }

  @Override
  public boolean equals(final java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final AixmDoc aixmDoc = (AixmDoc) o;
    return Objects.equals(this.identifier, aixmDoc.identifier)
        && Objects.equals(this.name, aixmDoc.name) && Objects.equals(this.version, aixmDoc.version);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.identifier, this.name);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("class AixmDoc {\n");

    sb.append("    identifier: ").append(toIndentedString(this.identifier)).append("\n");
    sb.append("    name: ").append(toIndentedString(this.name)).append("\n");
    sb.append("    version: ").append(toIndentedString(this.version)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(final java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
