/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import aero.facilis.schematronvalidator.model.Report;
import lombok.Data;

/**
 * @author khainglay
 * @author Thiha Maung
 *
 */
@Data
public class CombinedReport {

  private String datasetName;
  private String datasetChecksum;
  private String versionCode;
  private String userId;
  private String username;
  private UUID taskId;
  private Date date;
  private boolean isValid;
  private List<AixmSchema> schemaList = new ArrayList<>();
  private List<BizRule> bizRuleList = new ArrayList<>();

  private final List<Object> reportList = new ArrayList<>();

  public CombinedReport() {}

  public CombinedReport(final List<aero.facilis.xmlvalidator.model.Report> schemaReportList,
      final List<Report> schematronReportList) {
    this.reportList.addAll(schemaReportList);
    this.reportList.addAll(schematronReportList);
  }

  public boolean isValid() {
    this.isValid = this.reportList.isEmpty();
    return this.isValid;
  }

  public String convertSchemaListToString() {
    String result = "";
    final int size = this.schemaList.size();
    for (int i = 0; i < size; i++) {
      result = result.concat(this.schemaList.get(i).getName());
      if (i != size - 1) {
        result = result.concat(", ");
      }
    }
    return result;
  }

  public String convertBizRuleListToString() {
    String result = "";
    final int size = this.bizRuleList.size();
    for (int i = 0; i < size; i++) {
      result = result.concat(this.bizRuleList.get(i).getName());
      if (i != size - 1) {
        result = result.concat(", ");
      }
    }
    return result;
  }

  public String convertBizRuleListToStringForCert() {
    String result = this.convertBizRuleListToString();
    if (result.isEmpty()) {
      result = "None selected";
    }
    return result;
  }

  public String generateReportName() {
    return this.taskId + ".json";
  }

  public String generateCertificateName() {
    return this.taskId + ".pdf";
  }

}
