/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.model;

import java.util.Objects;
import java.util.UUID;
import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * InlineResponse200
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen",
    date = "2022-12-16T07:05:00.434Z[GMT]")
public class ValidationOption {

  @JsonProperty("identifier")
  @Valid
  private UUID identifier = null;

  @JsonProperty("name")
  @Valid
  private String name = null;
  
  @JsonProperty("title")
  @Valid
  private String title = null;

  public ValidationOption() {}

  public ValidationOption(@JsonProperty("identifier") final UUID identifier,
      @JsonProperty("name") final String name, @JsonProperty("title") final String title) {
    this.identifier = identifier;
    this.name = name;
    this.title = title;
  }

  /**
   * Get identifier
   *
   * @return identifier
   **/
  @Schema(description = "")
  @Valid
  public UUID getIdentifier() {
    return this.identifier;
  }

  public void setIdentifier(final UUID identifier) {
    this.identifier = identifier;
  }

  /**
   * Get name
   *
   * @return name
   **/
  @Schema(description = "")
  public String getName() {
    return this.name;
  }

  public void setName(final String name) {
    this.name = name;
  }
  
  /**
   * Get title
   *
   * @return title
   **/
  @Schema(description = "")
  public String getTitle() {
    return this.title;
  }

  public void setTitle(final String title) {
    this.title = title;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.identifier, this.name);
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(final java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
