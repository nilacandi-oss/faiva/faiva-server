/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * ValidationStatus
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen",
    date = "2022-12-16T07:05:00.434Z[GMT]")
public class ValidationStatus {
  /**
   * Gets or Sets status
   */
  public enum StatusEnum {
    IN_PROGRESS("Task is still in progress. Check again later."),

    VALID("Valid"),

    WARNINGS("Warnings"),

    INVALID("Invalid");

    private final String value;

    StatusEnum(final String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(this.value);
    }

    @JsonCreator
    public static StatusEnum fromValue(final String text) {
      for (final StatusEnum b : StatusEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }

  @JsonProperty("status")
  private StatusEnum status = null;

  @JsonProperty("messages")
  @Valid
  private List<String> messages = null;

  public ValidationStatus status(final StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * Get status
   *
   * @return status
   **/
  @Schema(description = "")

  public StatusEnum getStatus() {
    return this.status;
  }

  public void setStatus(final StatusEnum status) {
    this.status = status;
  }

  public ValidationStatus messages(final List<String> messages) {
    this.messages = messages;
    return this;
  }

  public ValidationStatus addMessagesItem(final String messagesItem) {
    if (this.messages == null) {
      this.messages = new ArrayList<String>();
    }
    this.messages.add(messagesItem);
    return this;
  }

  /**
   * Get messages
   *
   * @return messages
   **/
  @Schema(description = "")

  public List<String> getMessages() {
    return this.messages;
  }

  public void setMessages(final List<String> messages) {
    this.messages = messages;
  }


  @Override
  public boolean equals(final java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final ValidationStatus validationStatus = (ValidationStatus) o;
    return Objects.equals(this.status, validationStatus.status)
        && Objects.equals(this.messages, validationStatus.messages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.status, this.messages);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("class ValidationStatus {\n");

    sb.append("    status: ").append(toIndentedString(this.status)).append("\n");
    sb.append("    messages: ").append(toIndentedString(this.messages)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(final java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
