/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.model;

import aero.facilis.validator.exception.Key;

/**
 * @author khainglay
 * @author Thiha Maung
 *
 */
public enum ValidatorErrorMessages implements Key {
  INTERNAL_ERROR, NO_VALIDATION_OPTIONS_PROVIDED, SCHEMA_VERSION_ERROR, HEADER_ERROR, FILE_NOT_FOUND, IO_ERROR
}
