/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.model;

import lombok.Data;

/**
 * S3 file metadata that is assigned in validation report file
 * 
 * @author Thiha Maung
 *
 */
@Data
public class ReportMetadata {

  private final String name;
  private final String date;
  private final String result;
  private final String dataset;
  private final String version;
  private final String schema;
  private final String bizrule;
  private final String certificate;

}
