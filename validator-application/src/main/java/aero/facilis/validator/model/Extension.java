/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.model;

import java.util.Objects;
import java.util.UUID;
import org.springframework.validation.annotation.Validated;

/**
 * Extensions s
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen",
    date = "2022-12-16T07:05:00.434Z[GMT]")
public class Extension extends ValidationOption {

  public Extension(final UUID identifier, final String name, final String title) {
    super(identifier, name, title);
  }

  @Override
  public boolean equals(final java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode() {
    return Objects.hash(super.hashCode());
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("class Extensions {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(final java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
