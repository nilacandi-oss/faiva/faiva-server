/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;
import org.springframework.validation.annotation.Validated;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;

/**
 * AixmValidateBody
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen",
    date = "2022-12-16T07:05:00.434Z[GMT]")
public class AixmValidateBody {
  @JsonProperty("aixm-schema")
  @Valid
  private List<AixmSchema> aixmSchema = null;

  @JsonProperty("extensions")
  @Valid
  private List<Extension> extensions = null;

  @JsonProperty("biz-rules")
  @Valid
  private List<BizRule> bizRules = null;

  @JsonProperty("doc")
  private AixmDoc doc = null;

  public AixmValidateBody aixmSchema(final List<AixmSchema> aixmSchema) {
    this.aixmSchema = aixmSchema;
    return this;
  }

  public AixmValidateBody addAixmSchemaItem(final AixmSchema aixmSchemaItem) {
    if (this.aixmSchema == null) {
      this.aixmSchema = new ArrayList<AixmSchema>();
    }
    this.aixmSchema.add(aixmSchemaItem);
    return this;
  }

  /**
   * Get aixmSchema
   *
   * @return aixmSchema
   **/
  @Schema(description = "")
  @Valid
  public List<AixmSchema> getAixmSchema() {
    return this.aixmSchema;
  }

  public void setAixmSchema(final List<AixmSchema> aixmSchema) {
    this.aixmSchema = aixmSchema;
  }

  public AixmValidateBody extensions(final List<Extension> extensions) {
    this.extensions = extensions;
    return this;
  }

  public AixmValidateBody addExtensionsItem(final Extension extensionsItem) {
    if (this.extensions == null) {
      this.extensions = new ArrayList<Extension>();
    }
    this.extensions.add(extensionsItem);
    return this;
  }

  /**
   * Get extensions
   *
   * @return extensions
   **/
  @Schema(description = "")
  @Valid
  public List<Extension> getExtensions() {
    return this.extensions;
  }

  public void setExtensions(final List<Extension> extensions) {
    this.extensions = extensions;
  }

  public AixmValidateBody bizRules(final List<BizRule> bizRules) {
    this.bizRules = bizRules;
    return this;
  }

  public AixmValidateBody addBizRulesItem(final BizRule bizRulesItem) {
    if (this.bizRules == null) {
      this.bizRules = new ArrayList<BizRule>();
    }
    this.bizRules.add(bizRulesItem);
    return this;
  }

  /**
   * Get bizRules
   *
   * @return bizRules
   **/
  @Schema(description = "")
  @Valid
  public List<BizRule> getBizRules() {
    return this.bizRules;
  }

  public void setBizRules(final List<BizRule> bizRules) {
    this.bizRules = bizRules;
  }

  public AixmValidateBody doc(final AixmDoc doc) {
    this.doc = doc;
    return this;
  }

  /**
   * Get doc
   *
   * @return doc
   **/
  @Schema(description = "")

  @Valid
  public AixmDoc getDoc() {
    return this.doc;
  }

  public void setDoc(final AixmDoc doc) {
    this.doc = doc;
  }

  @Override
  public boolean equals(final java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final AixmValidateBody aixmValidateBody = (AixmValidateBody) o;
    return Objects.equals(this.aixmSchema, aixmValidateBody.aixmSchema)
        && Objects.equals(this.extensions, aixmValidateBody.extensions)
        && Objects.equals(this.bizRules, aixmValidateBody.bizRules)
        && Objects.equals(this.doc, aixmValidateBody.doc);
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.aixmSchema, this.extensions, this.bizRules, this.doc);
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("class AixmValidateBody {\n");

    sb.append("    aixmSchema: ").append(toIndentedString(this.aixmSchema)).append("\n");
    sb.append("    extensions: ").append(toIndentedString(this.extensions)).append("\n");
    sb.append("    bizRules: ").append(toIndentedString(this.bizRules)).append("\n");
    sb.append("    doc: ").append(toIndentedString(this.doc)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces (except the first line).
   */
  private String toIndentedString(final java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
