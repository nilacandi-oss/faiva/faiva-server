/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.properties;

import javax.validation.constraints.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import lombok.Data;

/**
 * DO metadata properties
 *
 * @author Thiha Maung
 *
 */
@Component
@ConfigurationProperties("do.metadata")
@Data
public class DOMetadataProperties {

  @NotNull
  private String name;
  @NotNull
  private String date;
  @NotNull
  private String result;
  @NotNull
  private String dataset;
  @NotNull
  private String version;
  @NotNull
  private String schema;
  @NotNull
  private String bizrule;
  @NotNull
  private String certificate;

}
