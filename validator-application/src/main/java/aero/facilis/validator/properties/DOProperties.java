/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.properties;

import javax.validation.constraints.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import lombok.Data;

/**
 * DO properties
 *
 * @author khainglay
 *
 */
@Component
@ConfigurationProperties("do.spaces")
@Data
public class DOProperties {

  @NotNull
  private String key;
  @NotNull
  private String secret;
  @NotNull
  private String endpoint;
  @NotNull
  private String region;
  @NotNull
  private String bucket;

  @Bean
  public AmazonS3 getS3() {
    final BasicAWSCredentials creds = new BasicAWSCredentials(this.key, this.secret);
    return AmazonS3ClientBuilder.standard()
        .withEndpointConfiguration(new EndpointConfiguration(this.endpoint, this.region))
        .withCredentials(new AWSStaticCredentialsProvider(creds)).build();
  }
}
