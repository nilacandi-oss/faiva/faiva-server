/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.properties;

import javax.validation.constraints.NotNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import lombok.Data;

/**
 * Validator Properties
 *
 * @author khainglay
 * @author Thiha Maung
 */
@Component
@ConfigurationProperties("validator.do")
@Data
public class ValidatorProperties {

  @NotNull
  private String documentPath;
  @NotNull
  private String validationOptionsPath;
  @NotNull
  private String schemaPath;
  @NotNull
  private String extensionPath;
  @NotNull
  private String bizRulePath;
  @NotNull
  private String validationHistoryPath;
  @NotNull
  private String certificatePath;

}
