/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import lombok.Data;

/**
 * Keycloak properties
 * 
 * @author Thiha Maung
 *
 */

@Component
@Data
public class KeycloakProperties {

  @Value("${keycloak.auth-server-url}")
  private String authSeverUrl;

  @Value("${keycloak.realm}")
  private String realm;

  @Value("${keycloak.resource}")
  private String resource;

  @Value("${keycloak.credentials.secret}")
  private String secret;
}
