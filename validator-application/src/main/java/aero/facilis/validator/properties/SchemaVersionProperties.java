/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import lombok.Data;

/**
 * Schema version properties
 *
 * @author Thiha Maung
 *
 */
@Component
@Data
public class SchemaVersionProperties {

  @Value("${schema.version.aixm.5-1-1.basic-msg-ns-url}")
  private String aixm511BasicMsgNsUrl;

  @Value("${schema.version.aixm.5-1-1.basic-msg-ns-url-code}")
  private String aixm511BasicMsgNsUrlCode;

  @Value("${schema.version.aixm.5-1.basic-msg-ns-url}")
  private String aixm51BasicMsgNsUrl;

  @Value("${schema.version.aixm.5-1.basic-msg-ns-url-code}")
  private String aixm51BasicMsgNsUrlCode;
}
