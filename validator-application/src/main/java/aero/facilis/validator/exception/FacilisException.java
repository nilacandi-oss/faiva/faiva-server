/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.exception;

/**
 * An interface to identify exceptions derived from InternalException and UserException. Facilis
 * exceptions store the message key for translation as the Exception message. They also store
 * translation message arguments for later translation.
 *
 * @author Benoit Maisonny
 *
 */
public interface FacilisException {

  /**
   * @return The Key of this message, to use for translation.
   */
  Key getKey();
}
