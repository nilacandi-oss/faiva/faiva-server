/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.exception;

import org.springframework.http.HttpStatus;
import lombok.Data;

/**
 * Represents an error response.
 * 
 * @author Thiha Maung
 *
 */
@Data
public class ErrorResponse {

  private final HttpStatus status;
  private final String message;

}
