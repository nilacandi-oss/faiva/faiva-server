/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.exception;

/**
 * A type of UserException for a problem that might not last. The user may want to try again after a
 * short while.
 *
 * @author Benoit Maisonny
 *
 */
public class TransientUserException extends UserException {

  /**
   *
   */
  private static final long serialVersionUID = 1L;


  public TransientUserException(final Key key) {
    super(key);
  }

  public TransientUserException(final Key key, final Throwable t) {
    super(key, t);
  }

  public TransientUserException(final Key key, final Object[] args) {
    super(key, args);
  }

  public TransientUserException(final Key key, final Throwable t, final Object[] args) {
    super(key, t, args);
  }
}
