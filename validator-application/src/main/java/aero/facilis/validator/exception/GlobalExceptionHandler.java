/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.amazonaws.AmazonServiceException;
import lombok.extern.log4j.Log4j2;

/**
 * Handle exceptions globally
 * 
 * @author Thiha Maung
 *
 */
@Log4j2
@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(InternalException.class)
  public ResponseEntity<ErrorResponse> handleInternalException(InternalException e) {

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    log.info(e);
    ErrorResponse errResponse =
        new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
    return new ResponseEntity<>(errResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(aero.facilis.xmlvalidator.exception.InternalException.class)
  public ResponseEntity<ErrorResponse> handleInternalException(
      aero.facilis.xmlvalidator.exception.InternalException e) {

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    log.info(e);
    ErrorResponse errResponse =
        new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
    return new ResponseEntity<>(errResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(aero.facilis.schematronvalidator.exception.InternalException.class)
  public ResponseEntity<ErrorResponse> handleInternalException(
      aero.facilis.schematronvalidator.exception.InternalException e) {

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    log.info(e);
    ErrorResponse errResponse =
        new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getLocalizedMessage());
    return new ResponseEntity<>(errResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(UserException.class)
  public ResponseEntity<ErrorResponse> handleUserException(UserException e) {

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    log.info(e);
    ErrorResponse errResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
    return new ResponseEntity<>(errResponse, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(aero.facilis.xmlvalidator.exception.UserException.class)
  public ResponseEntity<ErrorResponse> handleUserException(
      aero.facilis.xmlvalidator.exception.UserException e) {

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    log.info(e);
    ErrorResponse errResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
    return new ResponseEntity<>(errResponse, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(aero.facilis.schematronvalidator.exception.UserException.class)
  public ResponseEntity<ErrorResponse> handleUserException(
      aero.facilis.schematronvalidator.exception.UserException e) {

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    log.info(e);
    ErrorResponse errResponse = new ErrorResponse(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
    return new ResponseEntity<>(errResponse, HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(AmazonServiceException.class)
  public ResponseEntity<ErrorResponse> handleAmazonServiceException(AmazonServiceException e) {

    final HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);
    log.info(e);
    ErrorResponse errResponse =
        new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getErrorCode());
    return new ResponseEntity<>(errResponse, headers, HttpStatus.INTERNAL_SERVER_ERROR);
  }
}
