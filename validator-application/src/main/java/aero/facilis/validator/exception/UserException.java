/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.exception;

/**
 * Generic exception for a situation that can probably be solved by the user (as opposed to a system
 * administrator or developer). The solution may be to try again (transient error) or to correct
 * erroneous parameters.
 *
 * @author Benoit Maisonny
 *
 */
public class UserException extends Exception implements FacilisException {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  private final Key key;
  private final Object[] args;
  private String placeHolder;

  public UserException(final Key key) {
    this(key, (Throwable) null);
  }

  public UserException(final Key key, final Object[] args) {
    this(key, null, args);
  }

  public UserException(final Key key, final Throwable t) {
    this(key, t, null);
  }

  public UserException(final Key key, final Throwable t, final Object[] args) {
    super(key.toString(), t);
    this.key = key;
    // TODO Should we make a private copy?
    this.args = args;
  }

  public UserException(final Key key, final String placeHolder) {
    this(key, (Throwable) null);
    this.placeHolder = placeHolder;
  }

  /**
   * @return The message key, which is used to obtain a localized message.
   */
  @Override
  public Key getKey() {
    return this.key;
  }

  @Override
  public String getLocalizedMessage() {
    return getKey().translate(this.args, this.placeHolder);
  }

}
