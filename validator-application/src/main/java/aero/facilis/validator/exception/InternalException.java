/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.exception;

/**
 * Generic exception for internal errors, which cannot be solved by the user. For these exceptions,
 * most probably it is best to shut down the application and contact a system administrator, or even
 * Facilis helpdesk. This is an unchecked exception.
 *
 * @author Benoit Maisonny
 *
 */
public class InternalException extends RuntimeException implements FacilisException {
  /**
   *
   */
  private static final long serialVersionUID = 1L;

  private final Key key;
  private final Object[] args;

  public InternalException(final Key key) {
    this(key, (Throwable) null);
  }

  public InternalException(final Key key, final Object[] args) {
    this(key, null, args);
  }

  public InternalException(final Key key, final Throwable t) {
    this(key, t, null);
  }

  public InternalException(final Key key, final Throwable t, final Object[] args) {
    super(key.toString(), t);
    this.key = key;
    // TODO Should we make a private copy?
    this.args = args;
  }

  /**
   * @return The message key, which is used to obtain a localised message.
   */
  @Override
  public Key getKey() {
    return this.key;
  }


  @Override
  public String getLocalizedMessage() {
    return getKey().translate(this.args);
  }
}
