/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.api;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import aero.facilis.validator.exception.InternalException;
import aero.facilis.validator.exception.UserException;
import aero.facilis.validator.model.AixmDoc;
import aero.facilis.validator.model.AixmValidateBody;
import aero.facilis.validator.model.AixmValidateResponse;
import aero.facilis.validator.model.CombinedReport;
import aero.facilis.validator.model.ReportMetadata;
import aero.facilis.validator.model.ValidatorErrorMessages;
import aero.facilis.validator.service.AsyncTasksManager;
import aero.facilis.validator.service.ValidatorService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen",
    date = "2022-12-16T07:05:00.434Z[GMT]")
@RestController
@Log4j2
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class AixmApiController implements AixmApi {

  private final ValidatorService validatorService;
  private final HttpServletRequest request;
  private final AsyncTasksManager asyncTasksManager;

  private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(4);

  @Override
  public ResponseEntity<AixmDoc> uploadAIXMDocument(@Parameter(in = ParameterIn.DEFAULT,
      description = "", schema = @Schema()) @Valid @RequestBody final MultipartFile file)
      throws UserException {
    final String accept = this.request.getHeader("Accept");
    if (accept != null && accept.contains("application/json")) {
      final AixmDoc aixmdoc = this.validatorService.uploadDocument(file);
      log.debug("Aixm doc ID returning from controller: " + aixmdoc.getIdentifier());

      return new ResponseEntity<AixmDoc>(aixmdoc, HttpStatus.CREATED);
    } else {
      throw new UserException(ValidatorErrorMessages.HEADER_ERROR);
    }
  }

  @Override
  public CompletableFuture<ResponseEntity<AixmValidateResponse>> validate(@Parameter(
      in = ParameterIn.DEFAULT,
      description = "Json representation of user selected schema, extensions and business rules.",
      required = true, schema = @Schema()) @Valid @RequestBody final AixmValidateBody body)
      throws UserException, aero.facilis.xmlvalidator.exception.UserException {

    log.debug("Received request for asynchronous validation processing.");
    final String accept = this.request.getHeader("Accept");
    if (accept != null && accept.contains("application/json")) {

      // Check if a validation option is provided at least
      if (body.getAixmSchema() != null && body.getAixmSchema().isEmpty()
          && body.getExtensions() != null && body.getExtensions().isEmpty()
          && body.getBizRules() != null && body.getBizRules().isEmpty()) {
        throw new UserException(ValidatorErrorMessages.NO_VALIDATION_OPTIONS_PROVIDED);
      }
      final CompletableFuture<ResponseEntity<AixmValidateResponse>> response =
          new CompletableFuture<>();

      final String userId = this.getUserIdFromHttpServletRequest();
      final String username = this.validatorService.getKeycloakUsername(userId);

      final UUID taskId = UUID.randomUUID();
      log.debug("Task-id {} submitted for validation processing. Returning from controller.",
          taskId);

      // Return TaskId
      final AixmValidateResponse aixmValidateRes = new AixmValidateResponse(taskId,
          "Task has been started. Check status using /status/" + taskId);
      response.complete(
          ResponseEntity.accepted().header("Location", "/status/" + taskId).body(aixmValidateRes));

      // Capture the current thread context
      final ClassLoader threadContextThreadLoader = Thread.currentThread().getContextClassLoader();

      // Validating asynchronously
      final Runnable runnableTask = () -> {
        log.info("Validation ID - " + taskId + " is running asynchronously");
        // Set the retrieved thread context
        Thread.currentThread().setContextClassLoader(threadContextThreadLoader);

        CombinedReport combinedReport = null;
        try {
          combinedReport = this.validatorService.validateXMLWithSchemaAndPureSchematron(body,
              taskId, userId, username);

        } catch (IOException | aero.facilis.xmlvalidator.exception.UserException
            | URISyntaxException e) {
          throw new InternalException(ValidatorErrorMessages.INTERNAL_ERROR, e);
        }
        final CompletableFuture<CombinedReport> combinedFuture =
            CompletableFuture.completedFuture(combinedReport);
        this.asyncTasksManager.putTask(taskId, combinedFuture);
        log.info("Validation ID - " + taskId + " is done");
      };

      EXECUTOR_SERVICE.execute(runnableTask);

      return response;

    } else {
      throw new UserException(ValidatorErrorMessages.HEADER_ERROR);
    }
  }

  @Override
  public ResponseEntity<List<ReportMetadata>> getValidationHistory() throws UserException {

    final String accept = this.request.getHeader("Accept");
    if (accept != null && accept.contains("application/json")) {

      final String userId = this.getUserIdFromHttpServletRequest();
      final List<ReportMetadata> resultList = this.validatorService.getValidationHistory(userId);

      return new ResponseEntity<List<ReportMetadata>>(resultList, HttpStatus.OK);
    } else {
      throw new UserException(ValidatorErrorMessages.HEADER_ERROR);
    }
  }

  @Override
  public ResponseEntity<CombinedReport> getValidationReport(
      @Parameter(in = ParameterIn.PATH, description = "", required = true,
          schema = @Schema()) @PathVariable("reportFileName") final String reportFileName)
      throws IOException, UserException {

    final String accept = this.request.getHeader("Accept");
    if (accept != null && accept.contains("application/json")) {

      final String userId = this.getUserIdFromHttpServletRequest();
      final CombinedReport result =
          this.validatorService.getValidationReport(userId, reportFileName);

      return new ResponseEntity<CombinedReport>(result, HttpStatus.OK);
    } else {
      throw new UserException(ValidatorErrorMessages.HEADER_ERROR);
    }
  }

  @Override
  public ResponseEntity<Resource> getCertificate(
      @Parameter(in = ParameterIn.PATH, description = "", required = true,
          schema = @Schema()) @PathVariable("certificateFileName") final String certificateFileName)
      throws IOException, UserException {

    final String accept = this.request.getHeader("Accept");
    if (accept != null && accept.contains("application/pdf")) {

      final String userId = this.getUserIdFromHttpServletRequest();

      final byte[] fileByteArr = this.validatorService.getCertificate(userId, certificateFileName);
      final Resource fileResource = new ByteArrayResource(fileByteArr);
      final String contentType = "application/pdf";

      final HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.parseMediaType(contentType));
      headers.setContentDispositionFormData("attachment", certificateFileName);

      return new ResponseEntity<Resource>(fileResource, headers, HttpStatus.OK);

    } else {
      throw new UserException(ValidatorErrorMessages.HEADER_ERROR);
    }
  }

  private String getUserIdFromHttpServletRequest() {
    final KeycloakAuthenticationToken principal =
        (KeycloakAuthenticationToken) this.request.getUserPrincipal();
    return principal.getAccount().getKeycloakSecurityContext().getToken().getSubject();
  }

}
