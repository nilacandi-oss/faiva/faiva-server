/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.api;

import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import aero.facilis.validator.exception.UserException;
import aero.facilis.validator.model.ValidationOption;
import aero.facilis.validator.model.ValidatorErrorMessages;
import aero.facilis.validator.service.ValidatorService;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen",
    date = "2022-12-16T07:05:00.434Z[GMT]")
@RestController
@Log4j2
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class ValidationOptionsApiController implements ValidationOptionsApi {

  private final ValidatorService validatorService;
  private final HttpServletRequest request;

  @Override
  public ResponseEntity<Set<ValidationOption>> getValidationOptions() throws UserException {
    final String accept = this.request.getHeader("Accept");
    if (accept != null && accept.contains("application/json")) {
      final Set<ValidationOption> options = this.validatorService.getValidationOptions();
      log.debug("Validation Options returning from controller. " + options.size());
      return new ResponseEntity<Set<ValidationOption>>(options, HttpStatus.OK);
    } else {
      throw new UserException(ValidatorErrorMessages.HEADER_ERROR);
    }
  }

  @Override
  public ResponseEntity<Set<ValidationOption>> getValidationOptionsSchema(
      @Parameter(in = ParameterIn.PATH, description = "", required = true,
          schema = @Schema()) @PathVariable("versionCode") final String versionCode)
      throws UserException {
    final String accept = this.request.getHeader("Accept");
    if (accept != null && accept.contains("application/json")) {
      final Set<ValidationOption> options =
          this.validatorService.getValidationOptions(true, versionCode);
      log.debug("Validation Options (Only schemas) returning from controller. " + options.size());
      return new ResponseEntity<Set<ValidationOption>>(options, HttpStatus.OK);
    } else {
      throw new UserException(ValidatorErrorMessages.HEADER_ERROR);
    }
  }

  @Override
  public ResponseEntity<Set<ValidationOption>> getValidationOptionsBizRule(
      @Parameter(in = ParameterIn.PATH, description = "", required = true,
          schema = @Schema()) @PathVariable("versionCode") final String versionCode)
      throws UserException {
    final String accept = this.request.getHeader("Accept");
    if (accept != null && accept.contains("application/json")) {
      final Set<ValidationOption> options =
          this.validatorService.getValidationOptions(false, versionCode);
      log.debug("Validation Options (Only business rulesets) returning from controller. "
          + options.size());
      return new ResponseEntity<Set<ValidationOption>>(options, HttpStatus.OK);
    } else {
      throw new UserException(ValidatorErrorMessages.HEADER_ERROR);
    }
  }

}
