/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.api;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import aero.facilis.validator.exception.InternalException;
import aero.facilis.validator.exception.UserException;
import aero.facilis.validator.model.ValidationStatus;
import aero.facilis.validator.model.ValidatorErrorMessages;
import aero.facilis.validator.service.AsyncTasksManager;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen",
    date = "2022-12-16T07:05:00.434Z[GMT]")
@RestController
@Log4j2
@RequiredArgsConstructor
@CrossOrigin(origins = "*")
public class StatusApiController implements StatusApi {

  private final HttpServletRequest request;
  private final AsyncTasksManager asyncTasksManager;

  @Override
  public ResponseEntity<Object> getStatus(@Parameter(in = ParameterIn.PATH, description = "",
      required = true, schema = @Schema()) @PathVariable("taskId") final UUID taskId)
      throws UserException {
    final String accept = this.request.getHeader("Accept");
    if (accept != null && accept.contains("application/json")) {

      final CompletableFuture<? extends Object> futureResult =
          this.asyncTasksManager.getTask(taskId);

      if (futureResult == null) {
        return ResponseEntity.status(HttpStatus.ACCEPTED)
            .body("{\"Status\":\"Cannot find task for the moment.\"}");
      }
      if (futureResult.isDone()) {
        this.asyncTasksManager.removeTask(taskId);
        Object result = null;
        try {
          result = futureResult.get();
        } catch (InterruptedException | ExecutionException e) {
          log.error(e.getLocalizedMessage());
          throw new InternalException(ValidatorErrorMessages.INTERNAL_ERROR, e);
        }
        return ResponseEntity.ok().body(result);
      } else {
        return ResponseEntity.status(HttpStatus.PROCESSING)
            .body(ValidationStatus.StatusEnum.IN_PROGRESS);
      }
    } else {
      throw new UserException(ValidatorErrorMessages.HEADER_ERROR);
    }
  }

}
