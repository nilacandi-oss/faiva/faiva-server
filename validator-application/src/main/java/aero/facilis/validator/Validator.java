/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator;

import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import aero.facilis.validator.configuration.LocalDateConverter;
import aero.facilis.validator.configuration.LocalDateTimeConverter;
import springfox.documentation.oas.annotations.EnableOpenApi;

@SpringBootApplication
@EnableOpenApi
@ComponentScan(basePackages = {"aero.facilis.validator", "aero.facilis.validator.api",
    "aero.facilis.validator.configuration"})
public class Validator extends SpringBootServletInitializer {

  // @Override
  // public void run(final String... arg0) throws Exception {
  // if (arg0.length > 0 && arg0[0].equals("exitcode")) {
  // throw new ExitException();
  // }
  // }

  public static void main(final String[] args) {
    new SpringApplication(Validator.class).run(args);
  }

  @Configuration
  static class CustomDateConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addFormatters(final FormatterRegistry registry) {
      registry.addConverter(new LocalDateConverter("yyyy-MM-dd"));
      registry.addConverter(new LocalDateTimeConverter("yyyy-MM-dd'T'HH:mm:ss.SSS"));
    }
  }

  class ExitException extends RuntimeException implements ExitCodeGenerator {
    private static final long serialVersionUID = 1L;

    @Override
    public int getExitCode() {
      return 10;
    }

  }
}
