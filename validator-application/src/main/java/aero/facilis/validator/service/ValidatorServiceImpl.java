/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi All rights reserved.
 *
 * This source code is licensed under the license found in the LICENSE.txt file in the root
 * directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermissions;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import javax.validation.Valid;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSString;
import org.apache.pdfbox.pdfparser.PDFStreamParser;
import org.apache.pdfbox.pdfwriter.ContentStreamWriter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDStream;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import com.amazonaws.AmazonClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.amazonaws.services.s3.transfer.MultipleFileDownload;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import aero.facilis.schematronvalidator.api.SchematronValidator;
import aero.facilis.schematronvalidator.model.Report;
import aero.facilis.validator.exception.InternalException;
import aero.facilis.validator.exception.UserException;
import aero.facilis.validator.model.AixmDoc;
import aero.facilis.validator.model.AixmSchema;
import aero.facilis.validator.model.AixmValidateBody;
import aero.facilis.validator.model.BizRule;
import aero.facilis.validator.model.CombinedReport;
import aero.facilis.validator.model.Extension;
import aero.facilis.validator.model.ReportMetadata;
import aero.facilis.validator.model.ValidationOption;
import aero.facilis.validator.model.ValidatorErrorMessages;
import aero.facilis.validator.properties.DOMetadataProperties;
import aero.facilis.validator.properties.DOProperties;
import aero.facilis.validator.properties.KeycloakProperties;
import aero.facilis.validator.properties.SchemaVersionProperties;
import aero.facilis.validator.properties.ValidatorProperties;
import aero.facilis.xmlvalidator.api.XMLValidator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 * Implementation of Validator Service
 *
 * @author khainglay
 * @author Thiha Maung
 *
 */
@Service
@Log4j2
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ValidatorServiceImpl implements ValidatorService {

  private final AmazonS3 s3Client;
  private final DOProperties doProperties;
  private final DOMetadataProperties doMetadataProp;
  private final ValidatorProperties validatorProp;
  @Autowired
  private final SchemaVersionProperties schemaVarProp;
  private final KeycloakProperties kcProp;

  @Value("${validator.schemaDirectory}")
  private String schemaDirectory;

  @Override
  public AixmDoc uploadDocument(final MultipartFile file) throws UserException {
    // generate key: aixmDoc + newly generated document id + file name
    final UUID docId = UUID.randomUUID();
    final String fileName = file.getOriginalFilename();
    String aixmVersion = "";
    final ObjectMetadata metadata = this.extractMetadataOfFile(file);

    try {
      aixmVersion = this.readSchemaVersionFromFile(file);
      if (!StringUtils.isEmpty(aixmVersion)) {
        this.s3Client.putObject(new PutObjectRequest(this.doProperties.getBucket(),
            this.validatorProp.getDocumentPath() + "/" + docId + "/" + fileName,
            file.getInputStream(), metadata));
      } else {
        throw new UserException(ValidatorErrorMessages.SCHEMA_VERSION_ERROR);
      }
    } catch (final IOException | ParserConfigurationException | SAXException e) {
      throw new InternalException(ValidatorErrorMessages.INTERNAL_ERROR, e);
    }
    log.debug("FileName: " + fileName + ", Doc Id: " + docId + ", Version: " + aixmVersion);
    return new AixmDoc(docId, fileName, aixmVersion);
  }

  private ObjectMetadata extractMetadataOfFile(final MultipartFile file) {
    final ObjectMetadata metadata = new ObjectMetadata();

    try {
      final long contentLength = file.getInputStream().available();
      metadata.setContentLength(contentLength);
    } catch (final IOException e) {
      throw new InternalException(ValidatorErrorMessages.IO_ERROR, e);
    }

    if (file.getContentType() != null) {
      metadata.setContentType(file.getContentType());
    }

    log.info("Metadata of file: " + file.getOriginalFilename() + metadata);
    return metadata;
  }

  /**
   * Read schema version from root element
   * 
   * @param file
   * @return schemaVersion
   * @throws IOException
   * @throws ParserConfigurationException
   * @throws SAXException
   */
  private String readSchemaVersionFromFile(final MultipartFile file)
      throws IOException, ParserConfigurationException, SAXException, UserException {
    String version = "";
    if (file != null && file.getOriginalFilename().endsWith(".xml")) {
      final InputStream fileIS = file.getInputStream();

      final SAXParserFactory factory = SAXParserFactory.newInstance();
      final SAXParser parser = factory.newSAXParser();

      final SchemaVersionReaderHandler handler = new SchemaVersionReaderHandler();
      parser.parse(fileIS, handler);

      final String elemWithNsUrl = handler.getElemWithNsUrl();
      if (elemWithNsUrl.equalsIgnoreCase(this.schemaVarProp.getAixm511BasicMsgNsUrl())) {
        version = this.schemaVarProp.getAixm511BasicMsgNsUrlCode();

      } else if (elemWithNsUrl.equalsIgnoreCase(this.schemaVarProp.getAixm51BasicMsgNsUrl())) {
        version = this.schemaVarProp.getAixm51BasicMsgNsUrlCode();
      }
    } else {
      throw new UserException(ValidatorErrorMessages.SCHEMA_VERSION_ERROR);
    }
    return version;
  }

  @Override
  public Set<ValidationOption> getValidationOptions() throws UserException {
    final List<S3ObjectSummary> validationOptions = this.s3Client
        .listObjects(this.doProperties.getBucket(), this.validatorProp.getValidationOptionsPath())
        .getObjectSummaries();

    final Set<ValidationOption> options = new HashSet<>();
    for (final S3ObjectSummary validationOption : validationOptions) {
      // validation_options/schema/AIXM-5.1.1/uuid/name
      final String[] key = validationOption.getKey().split("/");
      final String type = key[1];
      final UUID id = UUID.fromString(key[3]);
      final String name = key[4];
      String title = "";
      final S3Object s3Obj =
          this.s3Client.getObject(this.doProperties.getBucket(), validationOption.getKey());
      final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      try {
        final DocumentBuilder builder = factory.newDocumentBuilder();
        final Document document = builder.parse(s3Obj.getObjectContent());
        document.getDocumentElement().normalize();

        title = document.getElementsByTagName("sch:title").item(0).getTextContent();

      } catch (IOException | ParserConfigurationException | SAXException e) {
        throw new UserException(ValidatorErrorMessages.IO_ERROR);
      }
      if (type.equals(this.validatorProp.getSchemaPath())) {
        options.add(new AixmSchema(id, name, ""));
      } else if (type.equals(this.validatorProp.getBizRulePath())) {
        options.add(new BizRule(id, name, title));
      } else {
        options.add(new Extension(id, name, title));
      }
    }

    return options;
  }

  @Override
  public Set<ValidationOption> getValidationOptions(final boolean isSchema,
      final String versionCode) throws UserException {

    // /schema/AIXM-5.1.1/
    String doSpacePath = "/";
    // Define path named schema or bizrule based on param isSchema
    doSpacePath = doSpacePath.concat(
        isSchema ? this.validatorProp.getSchemaPath() : this.validatorProp.getBizRulePath());
    doSpacePath = doSpacePath.concat("/" + versionCode + "/");

    final List<S3ObjectSummary> validationOptions =
        this.s3Client.listObjects(this.doProperties.getBucket(),
            this.validatorProp.getValidationOptionsPath() + doSpacePath).getObjectSummaries();

    final Set<ValidationOption> options = new HashSet<>();
    for (final S3ObjectSummary validationOption : validationOptions) {
      // validation_options/schema/AIXM-5.1.1/uuid/name
      final String[] key = validationOption.getKey().split("/");
      final String type = key[1];
      final UUID id = UUID.fromString(key[3]);
      final String name = key[4];
      String title = "";

      if (isSchema) {
        if (type.equals(this.validatorProp.getSchemaPath())) {
          options.add(new AixmSchema(id, name, title));
        } else {
          options.add(new Extension(id, name, title));
        }
      } else {
        final S3Object s3Obj =
            this.s3Client.getObject(this.doProperties.getBucket(), validationOption.getKey());
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
          final DocumentBuilder builder = factory.newDocumentBuilder();
          final Document document = builder.parse(s3Obj.getObjectContent());
          document.getDocumentElement().normalize();
          title = document.getElementsByTagName("sch:title").item(0).getTextContent();

        } catch (IOException | ParserConfigurationException | SAXException e) {
          throw new UserException(ValidatorErrorMessages.IO_ERROR);
        }

        if (type.equals(this.validatorProp.getBizRulePath())) {
          options.add(new BizRule(id, name, title));
        } else {
          options.add(new Extension(id, name, title));
        }
      }
    }

    return options;
  }

  @Override
  public CombinedReport validateXMLWithSchemaAndPureSchematron(final AixmValidateBody body,
      final UUID taskId, final String userId, final String username)
      throws aero.facilis.xmlvalidator.exception.UserException, IOException, URISyntaxException {

    final String datasetName = body.getDoc().getName();
    final String versionCode = body.getDoc().getVersion();
    final List<AixmSchema> schemaList = body.getAixmSchema();
    final List<BizRule> bizRuleList = body.getBizRules();
    final UUID datasetUuid = body.getDoc().getIdentifier();

    final Path datasetTmpPath = this.createFileDirInTmp(datasetName);
    final String datasetTmpPathStr = datasetTmpPath.toString();
    final File datasetFile = new File(datasetTmpPathStr);

    // aixmdocument/UUID/dataset
    final StringBuilder doSpacesUri = new StringBuilder();
    doSpacesUri.append(this.validatorProp.getDocumentPath() + "/").append(datasetUuid + "/")
        .append(datasetName);

    // Download from s3
    this.s3Client.getObject(
        new GetObjectRequest(this.doProperties.getBucket(), doSpacesUri.toString()), datasetFile);

    // Validate with XML schema
    final List<aero.facilis.xmlvalidator.model.Report> schemaReportList =
        this.validateXMLWithSchema(body, datasetFile);

    // Validate with Schematron
    final List<Report> schematronReportList = this.validateXMLWithPureSchematron(body, datasetFile);

    // Generate checksum
    final String datasetFileChecksum = DigestUtils.sha1Hex(new FileInputStream(datasetFile));

    final CombinedReport combinedReport =
        new CombinedReport(schemaReportList, schematronReportList);
    combinedReport.setDatasetName(datasetName);
    combinedReport.setDatasetChecksum(datasetFileChecksum);
    combinedReport.setVersionCode(versionCode);
    combinedReport.setUserId(userId);
    combinedReport.setUsername(username);
    combinedReport.setTaskId(taskId);
    combinedReport.setDate(new Date());
    combinedReport.setSchemaList(schemaList);
    combinedReport.setBizRuleList(bizRuleList);

    // Save validation report
    this.saveValidationReport(combinedReport);
    if (combinedReport.isValid()) {
      // Save certificate
      this.saveCertificate(combinedReport);
    }

    final String datasetTmpPathToDelete =
        datasetTmpPathStr.substring(0, datasetTmpPathStr.lastIndexOf('/'));
    deleteDirInTmp(new File(datasetTmpPathToDelete));

    return combinedReport;
  }

  @Override
  public List<aero.facilis.xmlvalidator.model.Report> validateXMLWithSchema(
      final AixmValidateBody body, final File datasetFile)
      throws aero.facilis.xmlvalidator.exception.UserException {

    final List<aero.facilis.xmlvalidator.model.Report> reports = new ArrayList<>();
    if (body.getAixmSchema() != null) {
      final @Valid List<AixmSchema> schemas = body.getAixmSchema();
      for (final AixmSchema schema : schemas) {
        // /var/lib/facilis/validator/schema/version/UUID/name
        final File schemaFile = new File(this.schemaDirectory + "/"
            + this.validatorProp.getSchemaPath() + "/" + body.getDoc().getVersion() + "/"
            + schema.getIdentifier() + "/" + schema.getName() + "/catalog.xml");
        final List<aero.facilis.xmlvalidator.model.Report> reportResult =
            XMLValidator.validateXMLWithSAX(datasetFile, schemaFile, schema.getName());
        reports.addAll(reportResult);
      }
    }
    return reports;
  }

  @Override
  public List<Report> validateXMLWithPureSchematron(final AixmValidateBody body,
      final File datasetFile) {

    final List<Report> reports = new ArrayList<>();
    if (body.getBizRules() != null) {
      final @Valid List<BizRule> rules = body.getBizRules();
      for (final BizRule rule : rules) {
        // /var/lib/facilis/validator/bizrule/version/UUID/name
        final File schematronFile =
            new File(this.schemaDirectory + "/" + this.validatorProp.getBizRulePath() + "/"
                + body.getDoc().getVersion() + "/" + rule.getIdentifier() + "/" + rule.getName());
        final List<Report> result =
            SchematronValidator.validateXMLWithPureSchematron(schematronFile, datasetFile);
        reports.addAll(result);
      }
    }
    return reports;
  }

  /**
   * Save validation report
   * 
   * @param combinedReport
   * @throws IOException
   */
  private void saveValidationReport(final CombinedReport combinedReport) throws IOException {

    final DateFormat dateFormatFull = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    final DateFormat dateFormatShort = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    final ObjectMapper objMapper = new ObjectMapper();
    objMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    objMapper.setDateFormat(dateFormatFull);

    final String userId = combinedReport.getUserId();
    final String reportName = combinedReport.generateReportName();
    final String certificateName = combinedReport.generateCertificateName();

    // Create report file in temp dir
    final Path reportTmpPath = this.createFileDirInTmp(reportName);
    final String reportTmpPathStr = reportTmpPath.toString();
    final File reportFile = new File(reportTmpPathStr);

    final byte[] reportByteArr = objMapper.writeValueAsBytes(combinedReport);
    try (FileOutputStream fileOutputStream = new FileOutputStream(reportFile);) {
      fileOutputStream.write(reportByteArr);
    }
    try (final InputStream reportFileInputStream = new FileInputStream(reportFile)) {

      final ObjectMetadata metadata = new ObjectMetadata();
      metadata.setContentType("application/json");
      metadata.setContentLength(reportFileInputStream.available());

      metadata.addUserMetadata(this.doMetadataProp.getName(), reportName);
      metadata.addUserMetadata(this.doMetadataProp.getDate(),
          dateFormatShort.format(combinedReport.getDate()));
      metadata.addUserMetadata(this.doMetadataProp.getResult(),
          combinedReport.isValid() ? "Valid" : "Invalid");
      metadata.addUserMetadata(this.doMetadataProp.getDataset(), combinedReport.getDatasetName());
      metadata.addUserMetadata(this.doMetadataProp.getVersion(), combinedReport.getVersionCode());
      metadata.addUserMetadata(this.doMetadataProp.getSchema(),
          combinedReport.convertSchemaListToString());
      metadata.addUserMetadata(this.doMetadataProp.getBizrule(),
          combinedReport.convertBizRuleListToString());
      if (combinedReport.isValid()) {
        metadata.addUserMetadata(this.doMetadataProp.getCertificate(), certificateName);
      }

      // validation_history/userId/taskId.json
      final StringBuilder doSpacesPath = new StringBuilder();
      doSpacesPath.append(this.validatorProp.getValidationHistoryPath() + "/").append(userId + "/")
          .append(reportName);

      // Upload to s3
      this.s3Client.putObject(new PutObjectRequest(this.doProperties.getBucket(),
          doSpacesPath.toString(), reportFileInputStream, metadata));
    }

    // Delete report file from temp dir
    final String reportTmpPathToDelete =
        reportTmpPathStr.substring(0, reportTmpPathStr.lastIndexOf('/'));
    deleteDirInTmp(new File(reportTmpPathToDelete));
  }

  /**
   * Save certificate
   * 
   * @param combinedReport
   * @throws IOException
   * @throws URISyntaxException
   */
  private void saveCertificate(final CombinedReport combinedReport)
      throws IOException, URISyntaxException {

    final String userId = combinedReport.getUserId();
    final String certificateName = combinedReport.generateCertificateName();

    // Create certificate file in temp dir
    final Path certificateTmpPath = this.createFileDirInTmp(certificateName);
    final String certificateTmpPathStr = certificateTmpPath.toString();
    final File certificateFile = new File(certificateTmpPathStr);
    final File certificateTemplate =
        new File(this.getClass().getResource("/certificate/CertificateTemplate.pdf").toURI());

    // Creating PDF document object
    try (PDDocument document = PDDocument.load(certificateTemplate)) {
      // Replace placeholders with actual data
      this.replacePlaceholders(document, combinedReport);
      // Implement certificate password protection
      this.implementCertificatePasswordProtection(document);
      document.save(certificateFile);
    }
    final ObjectMetadata metadata = new ObjectMetadata();
    metadata.setContentType("application/pdf");

    try (final InputStream certificateFileIS = new FileInputStream(certificateFile)) {

      metadata.setContentLength(certificateFileIS.available());
      // certificate/userId/taskId.pdf
      final StringBuilder doSpacesPath = new StringBuilder();
      doSpacesPath.append(this.validatorProp.getCertificatePath() + "/").append(userId + "/")
          .append(certificateName);
      // Upload to s3
      this.s3Client.putObject(new PutObjectRequest(this.doProperties.getBucket(),
          doSpacesPath.toString(), certificateFileIS, metadata));
    }
    // Delete certificate file from temp dir
    final String certificateTmpPathToDelete =
        certificateTmpPathStr.substring(0, certificateTmpPathStr.lastIndexOf('/'));
    deleteDirInTmp(new File(certificateTmpPathToDelete));
  }

  /**
   * Implement certificate password protection
   * 
   * @param document
   * @throws IOException
   */
  private void implementCertificatePasswordProtection(final PDDocument document)
      throws IOException {
    final AccessPermission accessPermission = new AccessPermission();
    accessPermission.setCanModify(false);
    final StandardProtectionPolicy policy =
        new StandardProtectionPolicy(UUID.randomUUID().toString(), "", accessPermission);
    document.protect(policy);
  }

  /**
   * Replace placeholders of loaded PDF template
   * 
   * @param document
   * @param combinedReport
   * @throws IOException
   */
  private void replacePlaceholders(final PDDocument document, final CombinedReport combinedReport)
      throws IOException {

    final DateFormat dateFormatShortUtc = new SimpleDateFormat("yyyy-MM-dd HH:mm 'UTC'");

    // Create a map to store the placeholders and their replacements
    final Map<String, String> replacements = new HashMap<>();
    replacements.put("{date}", dateFormatShortUtc.format(combinedReport.getDate()));
    replacements.put("{username}", combinedReport.getUsername());
    replacements.put("{datasetName}", combinedReport.getDatasetName());
    replacements.put("{datasetChecksum}", combinedReport.getDatasetChecksum());
    replacements.put("{versionCode}", combinedReport.getVersionCode());
    replacements.put("{schemaList}", combinedReport.convertSchemaListToString());
    replacements.put("{bizRuleList}", combinedReport.convertBizRuleListToStringForCert());

    // PDAcroForm form = document.getDocumentCatalog().getAcroForm()

    // Iterate through each page of the document
    for (final PDPage page : document.getDocumentCatalog().getPages()) {

      final PDFStreamParser parser = new PDFStreamParser(page);
      parser.parse();
      final List<Object> tokens = parser.getTokens();
      // Iterate through the tokens to find text strings and replace the placeholders
      for (int i = 0; i < tokens.size(); i++) {
        final Object next = tokens.get(i);
        if (next instanceof Operator) {
          final Operator op = (Operator) next;
          // Tj and TJ are the two operators that display strings in a PDF
          if (op.getName().equals("Tj")) {
            // Tj takes one operator, that is the string to display so lets update that operator
            final COSString previous = (COSString) tokens.get(i - 1);
            String string = previous.getString();
            boolean isValueChange = false;
            // Iterate through the placeholders and replace them in the text
            for (final Entry<String, String> replacement : replacements.entrySet()) {
              final String key = replacement.getKey();
              if (string.contains(key)) {
                string = string.replace(key, replacement.getValue());
                isValueChange = true;
              }
            }
            if (isValueChange) {
              previous.setValue(string.getBytes());
            }
          } else if (op.getName().equals("TJ")) {
            final COSArray previous = (COSArray) tokens.get(i - 1);

            for (int k = 0; k < previous.size(); k++) {
              final Object arrElement = previous.getObject(k);

              if (arrElement instanceof COSString) {
                final COSString cosString = (COSString) arrElement;
                String string = cosString.getString();
                boolean isValueChange = false;
                // Iterate through the placeholders and replace them in the text
                for (final Entry<String, String> replacement : replacements.entrySet()) {
                  final String key = replacement.getKey();
                  if (string.contains(key)) {
                    string = string.replace(key, replacement.getValue());
                    isValueChange = true;
                  }
                }
                if (isValueChange) {
                  cosString.setValue(string.getBytes());
                }
              }
            }
          }
        }
      }
      // The tokens are now updated, so we will replace the page content stream
      final PDStream updatedStream = new PDStream(document);
      final OutputStream out = updatedStream.createOutputStream();
      final ContentStreamWriter contentStreamWriter = new ContentStreamWriter(out);
      contentStreamWriter.writeTokens(tokens);
      page.setContents(updatedStream);
      out.close();
    }
  }

  @Override
  public String getKeycloakUsername(final String userId) {
    final RealmResource realmResource = this.getRealmResource();
    final UserResource userResource = realmResource.users().get(userId);
    final UserRepresentation userRepresentation = userResource.toRepresentation();
    return userRepresentation.getFirstName() + " " + userRepresentation.getLastName() + " ( "
        + userRepresentation.getUsername() + " )";
  }

  private RealmResource getRealmResource() {
    final Keycloak keycloak = KeycloakBuilder.builder().serverUrl(this.kcProp.getAuthSeverUrl())
        .realm(this.kcProp.getRealm()).grantType(OAuth2Constants.CLIENT_CREDENTIALS)
        .clientId(this.kcProp.getResource()).clientSecret(this.kcProp.getSecret())
        .resteasyClient(new ResteasyClientBuilder().connectionPoolSize(20).build()).build();

    return keycloak.realm(this.kcProp.getRealm());
  }

  @Override
  public List<ReportMetadata> getValidationHistory(final String userId) {

    final List<ReportMetadata> reportMetadataList = new ArrayList<>();
    // /userId/
    final String doSpacePath = "/" + userId + "/";
    final List<S3ObjectSummary> validationHistoryList =
        this.s3Client.listObjects(this.doProperties.getBucket(),
            this.validatorProp.getValidationHistoryPath() + doSpacePath).getObjectSummaries();

    for (final S3ObjectSummary history : validationHistoryList) {

      final ObjectMetadata objMetadata =
          this.s3Client.getObjectMetadata(this.doProperties.getBucket(), history.getKey());

      final String name = objMetadata.getUserMetaDataOf(this.doMetadataProp.getName());
      final String date = objMetadata.getUserMetaDataOf(this.doMetadataProp.getDate());
      final String result = objMetadata.getUserMetaDataOf(this.doMetadataProp.getResult());
      final String dataset = objMetadata.getUserMetaDataOf(this.doMetadataProp.getDataset());
      final String version = objMetadata.getUserMetaDataOf(this.doMetadataProp.getVersion());
      final String schema = objMetadata.getUserMetaDataOf(this.doMetadataProp.getSchema());
      final String bizrule = objMetadata.getUserMetaDataOf(this.doMetadataProp.getBizrule());
      final String cert = objMetadata.getUserMetaDataOf(this.doMetadataProp.getCertificate());
      final ReportMetadata reportMetadata =
          new ReportMetadata(name, date, result, dataset, version, schema, bizrule, cert);
      reportMetadataList.add(reportMetadata);
    }
    // Sort the result by date
    Collections.sort(reportMetadataList, Comparator.comparing(ReportMetadata::getDate).reversed());
    return reportMetadataList;
  }

  @Override
  public CombinedReport getValidationReport(final String userId, final String reportFileName)
      throws IOException {

    final Path reportFileTmpPath = this.createFileDirInTmp(reportFileName);
    final String reportFileTmpPathStr = reportFileTmpPath.toString();
    final File reportFile = new File(reportFileTmpPathStr);

    // validation_history/userId/reportFileName
    final StringBuilder doSpacesPath = new StringBuilder();
    doSpacesPath.append(this.validatorProp.getValidationHistoryPath() + "/").append(userId + "/")
        .append(reportFileName);

    // Download from s3
    this.s3Client.getObject(
        new GetObjectRequest(this.doProperties.getBucket(), doSpacesPath.toString()), reportFile);

    final DateFormat dateFormatFull = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
    final ObjectMapper objMapper = new ObjectMapper();
    objMapper.setDateFormat(dateFormatFull);
    final CombinedReport combinedReport = objMapper.readValue(reportFile, CombinedReport.class);

    final String reportFileTmpPathToDelete =
        reportFileTmpPathStr.substring(0, reportFileTmpPathStr.lastIndexOf('/'));
    deleteDirInTmp(new File(reportFileTmpPathToDelete));

    return combinedReport;
  }

  @Override
  public byte[] getCertificate(final String userId, final String certificateFileName) {
    byte[] fileByteArr = {};

    final Path certFileTmpPath = this.createFileDirInTmp(certificateFileName);
    final String certFileTmpPathStr = certFileTmpPath.toString();
    final File certFile = new File(certFileTmpPathStr);

    // certificate/userId/certificateFileName
    final StringBuilder doSpacesPath = new StringBuilder();
    doSpacesPath.append(this.validatorProp.getCertificatePath() + "/").append(userId + "/")
        .append(certificateFileName);

    // Download from s3
    this.s3Client.getObject(
        new GetObjectRequest(this.doProperties.getBucket(), doSpacesPath.toString()), certFile);

    try (final InputStream is = new FileInputStream(certFile)) {

      fileByteArr = IOUtils.toByteArray(is);

      final String certFileTmpPathToDelete =
          certFileTmpPathStr.substring(0, certFileTmpPathStr.lastIndexOf('/'));
      deleteDirInTmp(new File(certFileTmpPathToDelete));

    } catch (final IOException e) {
      throw new InternalException(ValidatorErrorMessages.IO_ERROR, e);
    }
    return fileByteArr;
  }

  /**
   * Download all objects from the virtual directory on s3 bucket
   *
   */
  @SuppressWarnings("unused")
  private String downloadObjects(@NonNull final String libraryUrl, @NonNull final UUID id,
      @NonNull final String name) {

    final Path filePath = this.createFileDirInTmp(null);
    final String filePathStr = filePath.toString();
    final File file = new File(filePathStr);

    final TransferManager xfer_mgr =
        TransferManagerBuilder.standard().withS3Client(this.s3Client).build();

    try {
      final MultipleFileDownload xfer = xfer_mgr.downloadDirectory(this.doProperties.getBucket(),
          libraryUrl + "/" + id + "/" + name, file);
      log.info(xfer.getDescription());
      xfer.waitForCompletion();
    } catch (final AmazonClientException | InterruptedException e) {
      throw new InternalException(ValidatorErrorMessages.INTERNAL_ERROR, e);
    }
    xfer_mgr.shutdownNow();

    return file.toString() + "/" + libraryUrl + "/" + id + "/" + name;
  }

  /**
   * Create file directory in tmp
   *
   * @param fileName
   * @return filePath
   */
  private Path createFileDirInTmp(final String fileName) {

    Path tmpFileDir = null;
    try {
      final Path tempDir = Files.createTempDirectory("validator-tmp-dir-" + UUID.randomUUID());
      Files.setPosixFilePermissions(tempDir, PosixFilePermissions.fromString("rwxrwxrwx"));
      if (fileName != null) {
        tmpFileDir = tempDir.resolve(fileName);
      } else {
        tmpFileDir = tempDir;
      }

    } catch (final IOException e) {
      throw new InternalException(ValidatorErrorMessages.IO_ERROR, e);
    }
    return tmpFileDir;
  }

  /**
   * Delete directory recursively
   *
   * @param dirToBeDeleted
   * @return boolean
   * @throws IOException
   */
  private boolean deleteDirInTmp(final File dirToBeDeleted) throws IOException {
    final File[] allFiles = dirToBeDeleted.listFiles();
    if (allFiles != null) {
      for (final File f : allFiles) {
        deleteDirInTmp(f);
      }
    }
    return Files.deleteIfExists(dirToBeDeleted.toPath());
  }
}
