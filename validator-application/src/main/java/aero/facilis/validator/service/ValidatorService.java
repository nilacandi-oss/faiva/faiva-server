/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.service;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.springframework.web.multipart.MultipartFile;
import aero.facilis.schematronvalidator.model.Report;
import aero.facilis.validator.exception.UserException;
import aero.facilis.validator.model.AixmDoc;
import aero.facilis.validator.model.AixmValidateBody;
import aero.facilis.validator.model.CombinedReport;
import aero.facilis.validator.model.ReportMetadata;
import aero.facilis.validator.model.ValidationOption;

/**
 * Validator Service
 *
 * @author khainglay
 * @author Thiha Maung
 *
 */
public interface ValidatorService {

  /**
   * Upload an aixm document.
   *
   * @param uploadFile
   * @return aixmDoc
   */
  public AixmDoc uploadDocument(final MultipartFile file) throws UserException;

  public Set<ValidationOption> getValidationOptions() throws UserException;

  /**
   * Get validation options based on version. If param isSchema is true, schemas are retrieved. Else
   * bizRules are retrieved.
   * 
   * @param isSchema
   * @param versionCode
   * @return ValidationOption Set
   * @throws UserException
   */
  public Set<ValidationOption> getValidationOptions(boolean isSchema, String versionCode)
      throws UserException;

  /**
   * Validate an aixm document using schame and pure schematron
   * 
   * @param AixmValidateBody that includes validation options and aixm document.
   * @return Result of validation
   * @throws UserException
   * @throws IOException
   */
  public CombinedReport validateXMLWithSchemaAndPureSchematron(final AixmValidateBody body,
      final UUID taskId, final String userId, final String username)
      throws aero.facilis.xmlvalidator.exception.UserException, IOException, URISyntaxException;

  /**
   * Validate an aixm document.
   *
   * @param AixmValidateBody that includes validation options and aixm document.
   * @param File downloaded file
   * @return Result of validation
   * @throws UserException
   */
  public List<aero.facilis.xmlvalidator.model.Report> validateXMLWithSchema(
      final AixmValidateBody body, final File datasetFile)
      throws aero.facilis.xmlvalidator.exception.UserException;

  /**
   * Validate an aixm document.
   *
   * @param AixmValidateBody that includes validation options and aixm document.
   * @param File downloaded file
   * @return Result of validation with reports
   */
  public List<Report> validateXMLWithPureSchematron(final AixmValidateBody body,
      final File datasetFile);

  /**
   * Get validation history.
   * 
   * @param userId
   * @return ReportMetadata list
   */
  public List<ReportMetadata> getValidationHistory(final String userId);

  /**
   * Get single validation report.
   * 
   * @param userId
   * @param reportFileName
   * @return CombinedReport
   * @throws IOException
   */
  public CombinedReport getValidationReport(final String userId, final String reportFileName)
      throws IOException;

  /**
   * Get certificate.
   * 
   * @param userId
   * @param certificateName
   * @return byte array
   */
  public byte[] getCertificate(final String userId, final String certificateFileName);

  /**
   * Get KC username using the given userId
   * 
   * @param userId
   * @return username
   */
  public String getKeycloakUsername(String userId);

}
