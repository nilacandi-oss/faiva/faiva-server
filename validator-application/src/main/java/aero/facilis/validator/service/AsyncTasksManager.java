/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.springframework.stereotype.Service;

/**
 * @author khainglay
 *
 */
@Service
public class AsyncTasksManager {

  private final ConcurrentMap<UUID, CompletableFuture<? extends Object>> mapOfTasks;

  public AsyncTasksManager() {
    this.mapOfTasks = new ConcurrentHashMap<>();
  }

  public void putTask(final UUID taskId, final CompletableFuture<? extends Object> dsf) {
    this.mapOfTasks.put(taskId, dsf);
  }

  public CompletableFuture<? extends Object> getTask(final UUID taskId) {
    return this.mapOfTasks.get(taskId);
  }

  public void removeTask(final UUID taskId) {
    this.mapOfTasks.remove(taskId);
  }

}
