/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.validator.service;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Custom handler for SAX parser to read schema version from uploaded document.
 * 
 * @author Thiha Maung
 *
 */
public class SchemaVersionReaderHandler extends DefaultHandler {

  private String element = "";
  private String namespaceUrl = "";
  private String elemWithNsUrl = "";

  private boolean isAixmBasicMsgElementFound;

  @Override
  public void startElement(final String uri, final String localName, final String qName,
      final Attributes attributes) {

    final String[] key = qName.split(":");

    if (!this.isAixmBasicMsgElementFound && key.length == 2) {

      // aixm-message:AIXMBasicMessage
      final String ns = key[0];
      final String elem = key[1];
      if (elem.equalsIgnoreCase("AIXMBasicMessage")) {
        this.element = qName;
        this.namespaceUrl = attributes.getValue("xmlns:" + ns);
        this.elemWithNsUrl = '{' + this.namespaceUrl + '}' + elem;
        this.isAixmBasicMsgElementFound = true;
      }
    } else if (!this.isAixmBasicMsgElementFound && key.length != 2
        && qName.equalsIgnoreCase("AIXMBasicMessage")) {

      this.element = qName;
      this.namespaceUrl = attributes.getValue("xmlns");
      this.elemWithNsUrl = '{' + this.namespaceUrl + '}' + qName;
      this.isAixmBasicMsgElementFound = true;
    }
  }

  public String getElement() {
    return this.element;
  }

  public String getNamespaceUrl() {
    return this.namespaceUrl;
  }

  public String getElemWithNsUrl() {
    return this.elemWithNsUrl;
  }
}
