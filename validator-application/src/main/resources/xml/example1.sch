<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron"
  xmlns:sqf="http://www.schematron-quickfix.com/validator/process" queryBinding="xslt2"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <sch:pattern>
    <!-- Title - styling elements are not allowed in title. -->
    <sch:rule context="title | shortdesc">
      <sch:report test="b" subject="b" role="info"> Bold is not allowed in <sch:name/>
        element.</sch:report>
    </sch:rule>

    <sch:rule context="li">
      <!-- The list item must not end with semicolon -->
      <sch:report test="ends-with(text()[last()], ';')" role="warn"> Semicolon is not allowed after list
        item.</sch:report>
    </sch:rule>
    
    <sch:rule context="text()">
      <sch:report test="matches(., '(http|www)\S+') 
        and local-name(parent::node()) != 'xref'" role="info">
        The link should be an an xref element</sch:report>
    </sch:rule>
  </sch:pattern>
</sch:schema>