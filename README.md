# faiva-server

FAIVA (FABEC Aeronautical Information Validator) is a Web application for verification and validation of aeronautical information, especially AIXM datasets.

This repository holds the front-end (FE), provides a user-friendly interface for users to view detailed validation reports and track validation history.

## Open source software

FAIVA is open source software developed by [Nilacandi](https://www.nilacandi.com/) and funded by [FABEC](https://www.fabec.eu/).

It is [licenced](LICENSE.txt) under the BSD 3-clause license. For a commercial license and support, please contact Nilacandi.

## 3rd Party software

Third-party libraries used in this application are listed in the file [FAIVA 3rd-party software.ods].
