/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.schematronvalidator.model;

import lombok.Data;

/**
 * @author khainglay
 * @author Thiha Maung
 *
 */
@Data
public class Report {

  private final String type = "Schematron";
  private final String schemaName;
  private final String errorMessage;
  private final String location;
  private final String flag;
  private final boolean isValid;

}
