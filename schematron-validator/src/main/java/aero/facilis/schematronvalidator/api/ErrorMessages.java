/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi
 * All rights reserved.
 *
 * This source code is licensed under the license found in the
 * LICENSE.txt file in the root directory of this source tree.
 *******************************************************************************/
package aero.facilis.schematronvalidator.api;

import aero.facilis.schematronvalidator.exception.Key;

/**
 * @author khainglay
 *
 */
public enum ErrorMessages implements Key {
  INTERNAL_ERROR, INVALID_BUSINESS_RULES
}
