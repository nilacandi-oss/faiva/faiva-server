/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi All rights reserved.
 *
 * This source code is licensed under the license found in the LICENSE.txt file in the root
 * directory of this source tree.
 *******************************************************************************/
package aero.facilis.schematronvalidator.api;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.transform.stream.StreamSource;
import org.springframework.stereotype.Service;
import com.helger.commons.collection.impl.ICommonsList;
import com.helger.schematron.ISchematronResource;
import com.helger.schematron.pure.SchematronResourcePure;
import com.helger.schematron.svrl.AbstractSVRLMessage;
import com.helger.schematron.svrl.SVRLFailedAssert;
import com.helger.schematron.svrl.SVRLHelper;
import com.helger.schematron.svrl.SVRLSuccessfulReport;
import com.helger.schematron.svrl.jaxb.SchematronOutputType;
import aero.facilis.schematronvalidator.exception.InternalException;
import aero.facilis.schematronvalidator.model.Report;

/**
 * Schematron Validator Implementation
 *
 * @author khainglay
 * @author Thiha Maung
 *
 */
@Service
public class SchematronValidator {

  // Utility classes should not have public constructors
  private SchematronValidator() {
    // throw new IllegalArgumentException("SchematronValidator")
  }

  public static List<Report> validateXMLWithPureSchematron(final File schematronFile,
      final File xmlFile) {

    List<Report> reports = new ArrayList<>();

    try {
      // Get schematron resource and check if it is valid
      final ISchematronResource schematronResource =
          SchematronResourcePure.fromFile(schematronFile);
      if (!schematronResource.isValidSchematron()) {
        throw new InternalException(ErrorMessages.INVALID_BUSINESS_RULES);
      }

      final BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(xmlFile));
      // Apply the Schematron validation on the passed XML resource and return a
      // SchematronOutputType object
      final SchematronOutputType output =
          schematronResource.applySchematronValidationToSVRL(new StreamSource(inputStream));
      final ICommonsList<SVRLSuccessfulReport> successfulReports =
          SVRLHelper.getAllSuccessfulReports(output);
      final ICommonsList<SVRLFailedAssert> failedAsserts =
          SVRLHelper.getAllFailedAssertions(output);

      // Handle successful reports
      if (!successfulReports.isEmpty()) {
        successfulReports.forEach(success -> {
          final AbstractSVRLMessage svrlMessage = success;
          final Report successReport = new Report(schematronFile.getName(), svrlMessage.getText(),
              svrlMessage.getLocation(), svrlMessage.getFlag().toString(), true);
          reports.add(successReport);
        });
      }

      // Handle fail reports
      if (!failedAsserts.isEmpty()) {
        failedAsserts.forEach(fail -> {
          final AbstractSVRLMessage svrlMessage = fail;
          final Report errorReport = new Report(schematronFile.getName(), svrlMessage.getText(),
              svrlMessage.getLocation(), svrlMessage.getFlag().toString(), false);
          reports.add(errorReport);
        });
      }
    } catch (final Exception e) {
      final Report errorReport = new Report(schematronFile.getName(),
          "Internal server error: " + e.getLocalizedMessage(), "", "ERROR", false);
      reports.add(errorReport);
    }
    return reports;
  }
}
