/*******************************************************************************
 * Copyright (c) 2024, Skeyes (on behalf of FABEC) and Nilacandi All rights reserved.
 *
 * This source code is licensed under the license found in the LICENSE.txt file in the root
 * directory of this source tree.
 *******************************************************************************/
package aero.facilis.schematronvalidator.api;

import static org.assertj.core.api.Assertions.assertThat;
import java.io.File;
import java.net.URL;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import aero.facilis.schematronvalidator.model.Report;

/**
 * @author khainglay
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SchematronValidator.class})
public class SchematronValidatorTest {

  /**
   * @throws java.lang.Exception
   */
  @Before
  public void setUp() throws Exception {}

  /**
   * @throws java.lang.Exception
   */
  @After
  public void tearDown() throws Exception {}

  /**
   * Test method for
   * {@link aero.facilis.schematronvalidator.api.SchematronValidator#validateXMLWithPureSchematron(java.io.InputStream, java.io.InputStream)}.
   */
  @Test
  public final void testValidateXMLWithPureSchematron_Invalid() {
    // prepare data
    final URL featureDocURL = this.getClass().getResource("/xml/example.xml");
    final File featureDoc = new File(featureDocURL.getFile());

    final URL invalidSchematronUrl = this.getClass().getResource("/xml/example.sch");
    final File invalidSchematron = new File(invalidSchematronUrl.getFile());

    final List<Report> result =
        SchematronValidator.validateXMLWithPureSchematron(invalidSchematron, featureDoc);

    boolean isValid = true;
    for (Report report : result) {
      if (!report.isValid()) {
        isValid = false;
        break;
      }
    }
    assertThat(isValid).isFalse();
  }

  /**
   * Test method for
   * {@link aero.facilis.schematronvalidator.api.SchematronValidator#validateXMLWithPureSchematron(java.io.InputStream, java.io.InputStream)}.
   */
  @Test
  public final void testValidateXMLWithPureSchematron_Valid() {
    // prepare data
    final URL featureDocURL = this.getClass().getResource("/xml/example.xml");
    final File featureDoc = new File(featureDocURL.getFile());

    final URL validSchematronUrl = this.getClass().getResource("/xml/example1.sch");
    final File validSchematron = new File(validSchematronUrl.getFile());

    final List<Report> result =
        SchematronValidator.validateXMLWithPureSchematron(validSchematron, featureDoc);

    boolean isValid = true;
    for (Report report : result) {
      if (!report.isValid()) {
        isValid = false;
        break;
      }
    }

    assertThat(isValid).isTrue();
  }

}
